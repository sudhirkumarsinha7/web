import * as React from "react";
import * as ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home  from "../Screens/home";
import About  from "../Screens/about";
import Contacts  from "../Screens/contact";
import Blogs  from "../Screens/blog";
import Layout  from "../Screens/Layout";

// import {
//   createBrowserRouter,
//   RouterProvider,
// } from "react-router-dom";
// const  router = createBrowserRouter([
//   {
//     path: "/",
//     element: <Root />,
//   },
// ]);
// export default router;

// ReactDOM.createRoot(document.getElementById("root")).render(
//   <React.StrictMode>
//     <RouterProvider router={router} />
//   </React.StrictMode>
// );

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
      <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="about" element={<About />} />

          <Route path="blogs" element={<Blogs />} />
          <Route path="contact" element={<Contacts />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);